extends "unit.gd" 
var key_move = {
	left=false,
	right=false,
	up=false, 
	down=false 
}
var key_attack = {
	kick=false
}
var key_block = 0

var curr_state	= {
	name 	= "", 
	fnc 	= null,
	extra 	= null
}


var is_walking 
var walking_transform 	= {direction=Vector3(),aim=Transform()}
onready var n_kick 		= get_node("skin/coll/kick")
onready var n_skin 		= get_node("skin")
onready var n_arms 		= get_node("skin/body/arms")
onready var n_ray 		= get_node("ray_down")
onready var name 		= get_name()

func _ready():
	randomize()
	gun_add(gun_all.kick)
	move_dir = n_skin.get_scale().x
	n_anim = get_node("skin/anim")
	foreach(self)
	
	gun_set(1)

########### DAMAGE #################
func damage(targ,dmg=1):
	d_print("damage "+str(targ))
	curr_state.fnc = null		#дропаем текущий стэйт
	set_state("state_damage",targ); return true

func state_damage():
	return play_anim("damage_"+curr_state.extra,0.7)

############ KICK #################
func state_kick():# стэйт ударов и блоков
	#print(gun.curr.damage)
	return gun.curr.shot(curr_state.extra.kick)

func ray_kick(extra):
	var obj
	if extra=="up": n_kick.set_translation(Vector3(1,6.5,0))
	elif extra=="down": n_kick.set_translation(Vector3(1,1.5,0))
	else: n_kick.set_translation(Vector3(1,4,0))
	n_kick.force_raycast_update()
	if n_kick.is_colliding():
		obj = n_kick.get_collider()
		get_kick_target(obj)
	if kick_target:
		if kick_target.has_method("damage"): kick_target.damage(obj.get_name(),1)
		kick_target = null
	return true
	
func state_idle(): 
	set_anim("idle",0.2,0.2); return true
	

func state_walk():
	set_anim("walk",curr_state.extra)
	return true

func set_state(state,extra=null):
	if curr_state.name == state: return 
	curr_state.name = state
	curr_state.fnc = funcref(self,state)
	curr_state.extra = extra
	d_print("set_state = "+str(state))

func update_state(): 
	if curr_state.fnc == null: return true
	if typeof(curr_state.fnc) != TYPE_OBJECT: return true
	return curr_state.fnc.call_func()

#====================== MOVEMENT ========================
func movement(state):
	linear_velocity = Vector3()
	walking_transform.direction = walking_transform.direction.normalized()
	if n_ray.is_colliding():
		linear_velocity = state.get_linear_velocity()
		var up = state.get_total_gravity().normalized()
		var normal = n_ray.get_collision_normal()
		var floor_velocity = Vector3()
		var object = n_ray.get_collider()
		if object extends RigidBody or object extends StaticBody:
			var point = n_ray.get_collision_point() - object.get_translation()
			var floor_angular_vel = Vector3()
			if object extends RigidBody:
				floor_velocity = object.get_linear_velocity()
				floor_angular_vel = object.get_angular_velocity()
			elif object extends StaticBody:
				floor_velocity = object.get_constant_linear_velocity()
				floor_angular_vel = object.get_constant_angular_velocity()
			var transform = Matrix3(Vector3(1, 0, 0), floor_angular_vel.x)
			transform = transform.rotated(Vector3(0, 1, 0), floor_angular_vel.y)
			transform = transform.rotated(Vector3(0, 0, 1), floor_angular_vel.z)
			floor_velocity += transform.xform_inv(point) - point
		var diff = floor_velocity + walking_transform.direction * walk_speed - linear_velocity
		var vertdiff = walking_transform.aim[1] * diff.dot(walking_transform.aim[1])
		diff -= vertdiff
		diff = diff.normalized() * clamp(diff.length(), 0, max_accel / state.get_step())
		diff += vertdiff
		apply_impulse(Vector3(), diff * get_mass())
		#if move.jump: _self.apply_impulse(Vector3(), normal * jump_speed * _self.get_mass())
	else: apply_impulse(Vector3(), walking_transform.direction * air_accel * get_mass())
	state.integrate_forces()
	walking_transform = {direction=Vector3(),aim=Transform()}

func walking():
	var d = Vector3()
	var a = get_global_transform().basis
	if key_move.up:  	d -= a[2]
	if key_move.down: 	d += a[2]
	if key_move.left: 	d -= a[0]
	if key_move.right:	d += a[0]
	return {direction=d,aim=a}

#предотвращаем избиение себя
func foreach(o):
	var obj = o.get_children()					#array[]
	for i in range(obj.size()):					#for 0 to obj
		if o extends KinematicBody || o extends RigidBody:
			exception(o)
		if obj[0] extends KinematicBody:
			exception(obj[0])
		foreach(obj[i])
func exception(o):
	#n_kick.add_exception(o)
	n_ray.add_exception(o)
	add_collision_exception_with(o)

func get_kick_target(o):
	if not o: return 
	if not o extends RigidBody:
		o = o.get_parent()
		get_kick_target(o)
	else: kick_target = o


#####################################################################
#------------------------------ GUNS  ------------------------------#
#####################################################################
var gun_all = {
	kick = load("res://scenes/guns/arm.tscn")
}
#--------------------
var gun_inv = [
]
#------список---------
var gun = {
	curr = null,
	n_anim = null,
	anim = ""
}

func gun_add(g):
	gun_inv.append(g)

func gun_inv_free(num):
	gun_inv.remove(num)

func gun_free(g):
	g.queue_free()
	gun.anim = ""
	gun.curr = null
	gun.n_anim = null

func gun_set(n):
	if gun_inv.size() < n: return
	if gun.curr: gun_free(gun.curr)
	var g = gun_inv[n-1].instance()
	n_arms.add_child(g)
	gun.curr = g
	gun.n_anim = g.get_node("anim")
