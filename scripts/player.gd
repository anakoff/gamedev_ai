extends "character.gd"

var input_states = load("res://scripts/input_states.gd")
var is_key_kick1 = input_states.new("kick1")
var is_key_block = input_states.new("block")

var timer = 0

func _ready():
	debug_print = true

func logic(s):
	key_attack.kick = is_key_kick1.check()
	key_block = is_key_block.check()
	is_walking = is_pressed_key("move_")
	if is_walking: check_walk()
	
	if not update_state(): return #если есть состояние, дальше не идем
	elif check_kick(): return
	elif check_block(): return
	else: set_state("state_idle")

func switch_key(key):
	if key=="left": return -1
	elif key=="right": return 1 
	#return 0

func check_walk():
	walking_transform = walking()
	if linear_velocity.x == 0: return 
	var abs_vel = abs(linear_velocity.x)
	var dir = abs_vel/linear_velocity.x
	set_dir(dir)
	if abs_vel >1 || abs(linear_velocity.z) >1:
		set_anim("walk")

func set_dir(dir):
	if dir == move_dir: return 
	var scale = n_skin.get_scale()
	n_skin.set_scale(Vector3(scale.x*-1,scale.y,scale.z))
	move_dir = dir

func check_kick(): #проверка ударов 
	for i in key_attack:
		if key_attack[i] ==1:
			i += str(randi()%2+1)+"_" # "kick1_"
			set_state( "state_kick",{kick=str(i)} );  return true

func check_block(): #проверка  блоков
		if key_block !=0:
			set_state( "state_kick",{kick="block_"} );  return true

#return pressed key name in key_array
func is_pressed_key(key_name):
	var k
	for i in key_move:
		key_move[i] = Input.is_action_pressed(key_name+str(i))
		if key_move[i]: k = i
	return k