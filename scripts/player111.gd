extends "character.gd"

onready var n_cam 		= get_node("camera")

var move_actions 		= ["right","left","down","up"]
var move_vectors 		= [Vector2(1,0),Vector2(-1,0),Vector2(0,1),Vector2(0,-1)]
var relative_mpos 		= Vector2()
var mouse_pos 			= Vector2()

func _ready():
	n_anim 		= get_node("skin/anim")
	n_anim_leg 	= get_node("skin/anim_leg")
	set_process(true)
	set_process_input(true)

func _process(d):
	var offset 		= -get_viewport().get_canvas_transform().o # Get the offset
	relative_mpos 	= mouse_pos + offset # And add it to the mouse position

func _input(event):
	if(event.type == InputEvent.MOUSE_MOTION): # When we move the mouse
		mouse_pos = event.pos # We change the position of it

func logic(delta):
	is_move = moved(delta)
	if is_move: set_anim_leg("walk")
	else: set_anim_leg("idle")

	if not update_state(): return 							#если выполняется стэйт, то выходим
	elif change_attack(): set_state("state_attack"); return
	elif is_move: set_state("state_walk"); return		
	else: set_state("state_idle")
	
func change_attack():
	if Input.is_action_pressed("attack1"): return true

func state_attack():
	return play_anim("kick_l")

func moved(d):
	lookat(relative_mpos)
	var move = false
	for i in range(4):
		if(Input.is_action_pressed(move_actions[i-1])):
			force += move_vectors[i-1]
			move = true
	return move
	#moveto(relative_mpos)