extends RigidBody
var debug_print = false
var walk_speed 	= 10
var max_accel	= 2
var air_accel 	= 0.5
export var health 	= 20
var delta 			= 0

var linear_velocity = Vector3()
var is_move 		= false 
var is_damage 		= false
var move_dir 		= 0

var n_anim
var target
var kick_target

var anim 			= {
	curr = "",
	speed = 1,
	blend = 0.0
}

func _integrate_forces(s):
	delta = s.get_step()
	logic(delta)
	movement(s) 


func logic(delta): pass 
func movement(s): pass 

func set_anim(name_anim,speed=anim.speed, blend=anim.blend):
	if not n_anim: return 
	if anim.curr == name_anim: return 
	anim.curr = name_anim
	n_anim.play(name_anim,blend,speed)

func play_anim(name_anim,speed=anim.speed,blend=anim.blend):
	set_anim(name_anim,speed,blend)
	var pos = n_anim.get_current_animation_pos()
	var len = n_anim.get_current_animation_length()-delta
	if pos >= len:
		return true

func d_print(text):
	if debug_print: print(text)